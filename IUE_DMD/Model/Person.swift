//
//  Person.swift
//  IUE_DMD
//
//  Created by Catarina Silva on 21/09/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit

class Person: NSObject, NSSecureCoding{
    
    static var supportsSecureCoding: Bool {
        return true
    }
    
    var name:String
    var email:String
    var age = 18
    var photo: String?
    var postalCode: PostalCode?
    var image: UIImage?
    
    init(name:String, email:String, age:Int) {
        self.name = name
        self.email = email
        self.age = age
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: Constants.NAME)
        aCoder.encode(email, forKey: Constants.EMAIL)
        aCoder.encode(age, forKey: Constants.AGE)
        aCoder.encode(photo, forKey: Constants.PHOTO)
        aCoder.encode(postalCode?.postalcode, forKey: Constants.POSTALCODE)
        aCoder.encode(postalCode?.placeName, forKey: Constants.PLACE_NAME)
        if let lat = postalCode?.lat {
            aCoder.encode(lat, forKey: Constants.LATITUDE)
        }
        if let lon = postalCode?.lng {
            aCoder.encode(lon, forKey: Constants.LONGITUDE)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: Constants.NAME) as! String
        email = aDecoder.decodeObject(forKey: Constants.EMAIL) as! String
        photo = aDecoder.decodeObject(forKey: Constants.PHOTO) as? String
        age = aDecoder.decodeInteger(forKey: Constants.AGE)
        
        if let pc = aDecoder.decodeObject(forKey: Constants.POSTALCODE) {
            let p = PostalCode(
                postalcode: pc as! String,
                placeName: aDecoder.decodeObject(forKey: Constants.PLACE_NAME) as! String,
                lng: aDecoder.decodeDouble(forKey: Constants.LONGITUDE),
                lat: aDecoder.decodeDouble(forKey: Constants.LATITUDE))
            self.postalCode = p
        }
    }
}
