//
//  PeopleRepository.swift
//  IUE_DMD
//
//  Created by Catarina Silva on 19/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

class PeopleRepository {
    
    static let repository = PeopleRepository()
    
    private init () {}
    
    var people = [Person]()
    let defaults = UserDefaults.standard

    func savePeople () {
        if let p = archivePersonArray(peopleDataArray: people) {
            defaults.set(p, forKey: Constants.PEOPLE_ARCHIVE)
            print("p: \(p)")
        }
    }
    
    func loadPeople (){
        let d = defaults.data(forKey: Constants.PEOPLE_ARCHIVE)
        if d != nil {
            people = loadPersonArray(unarchivedObject: d as! Data)
             print("d: \(d)")
        }
        
    }
    
    func archivePersonArray(peopleDataArray : [Person]) -> Data? {
        return try? NSKeyedArchiver.archivedData(withRootObject: peopleDataArray as Array, requiringSecureCoding: false)
    }
    func loadPersonArray(unarchivedObject: Data) -> [Person] {
        return try! NSKeyedUnarchiver.unarchivedObject(ofClasses: [NSArray.self, Person.self], from: unarchivedObject) as! [Person]
    }
    
}
