//
//  GeoNamesClient.swift
//  IUE_DMD
//
//  Created by Luis Marcelino on 04/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

class GeoNamesClient {
    // Methods to connect to GeoNames.or server
    
    static func postalCodeLookup(postalCode: String, country: String, completionHandler: @escaping ([PostalCode]?, Error?) -> Void ) {
        let url = URL(string: "http://api.geonames.org/postalCodeLookupJSON?country=" + country + "&username=livro_ios5&postalcode=" + postalCode)!
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completionHandler(nil, error)
                return
            }
//            guard let httpResponse = response as? HTTPURLResponse,
//                (200...299).contains(httpResponse.statusCode) else {
//                    self.handleServerError(response)
//                    return
//            }
            // process data
            var postalCodes = [PostalCode]()
            if let data = data {
                if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:Any] {
                    let postcodesArray = json["postalcodes"] as! [[String:Any]]
                    for postJson in postcodesArray {
                        if let pc = PostalCode.create(json: postJson) {
                            postalCodes.append(pc)
                        }
                    }
                }
            }
            // call completionHandler

            completionHandler(postalCodes, nil)
        }
        task.resume()
    }
}
