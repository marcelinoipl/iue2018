//
//  Constants.swift
//  IUE_DMD
//
//  Created by Luis Marcelino on 25/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

class Constants {
    static let NAME = "name"
    static let EMAIL = "email"
    static let AGE = "age"
    static let PHOTO = "photo"
    static let POSTALCODE = "postalcode"
    static let PLACE_NAME = "place_name"
    static let LATITUDE = "latitude"
    static let LONGITUDE = "longitude"
    static let INT_VALUE = "int_value"
    static let PEOPLE_ARCHIVE = "people_arch"
}
