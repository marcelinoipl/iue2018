//
//  PostalCode.swift
//  IUE_DMD
//
//  Created by Luis Marcelino on 04/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation


struct PostalCode {
    let postalcode: String
    let placeName: String
    let lng:Double
    let lat:Double
    
    static func create(json: [String:Any]) -> PostalCode? {
        let postal = PostalCode(
            postalcode: json["postalcode"] as! String,
            placeName: json["placeName"] as! String,
            lng: json["lng"] as! Double,
            lat: json["lat"] as! Double)
        return postal
    }

}
