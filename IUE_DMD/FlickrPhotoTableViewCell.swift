//
//  FlickrPhotoTableViewCell.swift
//  IUE_DMD
//
//  Created by Luis Marcelino on 18/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit
import SDWebImage

class FlickrPhotoTableViewCell: UITableViewCell {

    @IBOutlet weak var photoTitle: UILabel!
    @IBOutlet weak var photImage: UIImageView!
    var photo:FlickrPhoto! {
        didSet {
            photoTitle.text = photo.title
            
            photImage.sd_setImage(with: photo.url!, placeholderImage: UIImage(named: "first"))
            
//            FlickrClient.fetchImage(photo: photo) { (image, error) in
//                OperationQueue.main.addOperation {
//                    self.photImage.image = image
//                }
//            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
