//
//  PersonTableViewCell.swift
//  IUE_DMD
//
//  Created by Luis Marcelino on 28/09/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var photoImage: UIImageView!
    
    func setPhoto(photoName:String?) {
        if let name = photoName {
            if let photImage = UIImage(named: name) {
                photoImage.image = photImage
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
