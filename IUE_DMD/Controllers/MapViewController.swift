//
//  MapViewController.swift
//  IUE_DMD
//
//  Created by Catarina Silva on 12/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    //HOMEWORK FOR 19 OCTOBER -> SHOW ALL PEOPLE LOCATIONS ON MAP
    
    let locationManager = CLLocationManager()
    
    var currentLocation:CLLocationCoordinate2D?
    
    let annotation = MKPointAnnotation()
    
    
    @IBOutlet weak var map: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        map.delegate = self
        map.showsUserLocation = true
        
        
//        if let curLoc = currentLocation {
//            let region = MKCoordinateRegion(center: curLoc, latitudinalMeters: 10000, longitudinalMeters: 10000)
//            map.setRegion(region, animated: true)
//        }
        locationManager.delegate = self
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        
        locationManager.requestWhenInUseAuthorization()
        
        //locationManager.startUpdatingLocation()
        

        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        //}
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coordinate = locationObj?.coordinate
        
        if let coord = coordinate {
            print("latitude: \(coord.latitude) longitude: \(coord.longitude)")
            currentLocation = coord
            if let curLoc = currentLocation {
                let region = MKCoordinateRegion(center: curLoc, latitudinalMeters: 10000, longitudinalMeters: 10000)
                map.setRegion(region, animated: true)
                annotation.coordinate = curLoc
                annotation.title = "My current location"
                map.addAnnotation(annotation)
            }
        }
    }

    //MARK: - Gestures
    
    @IBAction func didTapMap(_ sender: UITapGestureRecognizer) {
        let tapPoint:CGPoint = sender.location(in: map)
        let location = map.convert(tapPoint, toCoordinateFrom: map)
        
        annotation.coordinate = location
        annotation.title = "Just tapped righht here"
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
