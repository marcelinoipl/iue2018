//
//  FirstViewController.swift
//  IUE_DMD
//
//  Created by Luis Marcelino on 20/09/18.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITextFieldDelegate,  UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var person:Person?
    
    @IBOutlet weak var myLabel: UILabel!
    
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var ageField: UITextField!

    @IBOutlet weak var postalCodeField: UITextField!

    @IBOutlet weak var photoName: UITextField!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var photoImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
                
//        let i = 10
//        print (i)
//        print("i: \(i)")
//        var j:Int?
//        print("j: \(j)")
//        j = 20
//        print("j: \(j)")
//
//        print("confident j: \(j!)")
//        if let unwrappedJ = j {
//            print("unwrappedJ: \(unwrappedJ)")
//        }
        
        self.person = Person(name: "", email: "", age: 0)
    }

    func showAlert(message:String, title:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //IBActions

    @IBAction func  savePerson(_ sender: Any) {
        
        if let n = nameField.text {
            if let e = emailField.text {
                if let a = ageField.text {
                    if let integerAge = Int(a) {
                        person?.name = n
                        person?.email = e
                        person?.age = integerAge
                        PeopleRepository.repository.people.append(person!)
                        
                        if let photo = photoName.text {
                            if !photo.isEmpty {
                                person!.photo = photo
                            }
                        }
                        
                        //showAlert(message: "Person saved!", title: "")
                        dismiss(animated: true, completion: nil)
                    } else {
                        showAlert(message: "All fields must be correct.", title: "")
                    }
                }
            }
            nameField.text = ""
            emailField.text = ""
            ageField.text = ""
        }
        
        PeopleRepository.repository.savePeople()
    }
    
    @IBAction func printPeople(_ sender: UIButton) {
        for p in PeopleRepository.repository.people {
            print("Name: \(p.name) Email:\(p.email) Age:\(p.age)")
        }
    }
    
    // MARK: ---- UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == postalCodeField {
            GeoNamesClient.postalCodeLookup(postalCode: postalCodeField.text!, country: "PT") { (postCodes, error) in
                OperationQueue.main.addOperation {
                    if error != nil {
                        let alert = UIAlertController(title: "Postal Code error", message: error!.localizedDescription, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                            NSLog("The \"OK\" alert occured.")
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else {
                        // create table contoller and show postal codes
                        let postalCodePicker = PostalCodePickerTableViewController()
                        postalCodePicker.postalCodes = postCodes!
                        postalCodePicker.person = self.person
                        self.navigationController?.pushViewController(postalCodePicker, animated: true)
                    }
                }
                
                
                
                print(postCodes)
            }
            textField.resignFirstResponder()
            return true;
        }
        if textField == nameField {
            emailField.becomeFirstResponder()
        }
        return false
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func takePicture(_ sender: Any) {
        let controller = UIImagePickerController()
        controller.sourceType =  UIImagePickerController.SourceType.photoLibrary
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        person?.image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.photoImage.image = person?.image
        picker.dismiss(animated: true, completion: nil)
    }
}

