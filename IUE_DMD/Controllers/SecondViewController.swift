//
//  SecondViewController.swift
//  IUE_DMD
//
//  Created by Luis Marcelino on 20/09/18.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    var person:Person?
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var ageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let p = person{
            print("name: \(p.name)")
            nameLabel.text = p.name
            emailLabel.text = p.email
            ageLabel.text = "\(p.age)"
        } else {
            print("No data")
        }
    }


}

