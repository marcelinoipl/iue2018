//
//  FlickrClient.swift
//  IUE_DMD
//
//  Created by Luis Marcelino on 18/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit

class FlickrClient {
    static func fetchImages (completionHandler: @escaping ([FlickrPhoto]?, Error?) -> Void) {
        let url = URL(string: "https://www.flickr.com/services/rest/?api_key=f9333373bf926daaef986f5251554e9e&format=json&method=flickr.photos.getRecent")!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let d = data {
                guard var jsonString = String(data: d, encoding: .utf8) else {
                    return
                }
                let startIndex = jsonString.index(jsonString.startIndex, offsetBy: 14)
                jsonString = String(jsonString[startIndex...])
                
                let endIndex = jsonString.index(jsonString.endIndex, offsetBy: -1)
                jsonString = String(jsonString[..<endIndex])

                do {
                    let data1 = jsonString.data(using: String.Encoding.utf8)!

                    if let json = try JSONSerialization.jsonObject(with: data1, options: .allowFragments) as? [String:Any]{
                        
                        guard let jsonPhotos = json["photos"] as? [String:Any] else {
                            completionHandler(nil, error)
                            return
                        }
                        guard let realJsonPhotos = jsonPhotos["photo"] as? [[String:Any]] else {
                            completionHandler(nil, error)
                            return
                        }
                        var photos = [FlickrPhoto]()
                        for jsonPhoto in realJsonPhotos {
                            if let p = FlickrPhoto.createPhoto(fromJson: jsonPhoto as! [String:Any]) {
                                photos.append(p)
                            }
                        }
                        completionHandler(photos, nil)
                    }
                }
                catch {
                    completionHandler(nil, error)
                }
                
            }
        }.resume()
    }
    
    static func fetchImage (photo: FlickrPhoto, completionHandler: @escaping ( UIImage?, Error?) -> Void) {
        guard let url = photo.url else {
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let d = data {
                let image = UIImage(data: d)
                completionHandler(image, nil)
            }
        }.resume()
    }
}
