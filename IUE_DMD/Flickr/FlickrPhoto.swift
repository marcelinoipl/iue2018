//
//  FlickrPhoto.swift
//  IUE_DMD
//
//  Created by Luis Marcelino on 18/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

struct FlickrPhoto {
    let id:String
    let secret:String
    let server:String
    let farm:Int
    let title: String
    
    static func createPhoto (fromJson json:[String:Any]) -> FlickrPhoto? {
        guard let id = json["id"] as? String else {
            return nil
        }
        guard let secret = json["secret"] as? String else {
            return nil
        }
        guard let farm = json["farm"] as? Int else {
            return nil
        }
        guard let server = json["server"] as? String else {
            return nil
        }
        guard let title = json["title"] as? String else {
            return nil
        }
        return FlickrPhoto(id: id, secret: secret, server: server, farm: farm, title: title)
    }
    
    var url:URL? {
        return URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret).jpg")
    }
}
