//
//  IUE_DMDTests.swift
//  IUE_DMDTests
//
//  Created by Luis Marcelino on 20/09/18.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import XCTest
@testable import IUE_DMD

class IUE_DMDTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    let jsonString = "{\"adminCode2\": \"1009\",\"adminName3\":\"Leiria\",\"adminCode1\": \"13\",\"adminName2\": \"Leiria\",\"lng\": -8.8070547580719,\"countryCode\": \"PT\",\"postalcode\": \"2410-052\",\"adminName1\": \"Leiria\",\"placeName\": \"Leiria\",\"lat\": 39.7436180100595}"

    let jsonResponse = [
        "postalcode": "2410-052",
        "placeName": "Leiria",
        "lng": -8.8070547580719,
        "lat": 39.7436180100595
        ] as [String : Any]
    func testPostalCode() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let postalcode = PostalCode.create(json: jsonResponse)
        XCTAssertEqual("Leiria", postalcode?.placeName)
        XCTAssertEqual("2410-052", postalcode?.postalcode)
    
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testFlickrParse() {
        let json: [String:Any] = [
            "id":"30466337097",
            "owner":"154310511@N02",
            "secret":"f4dcdc92a8",
            "server":"1921",
            "farm":2,
            "title":"Best Ideas For Diy Crafts : Learn about the Walt Disney World Birthday Freebies | Design Dazzle",
            "ispublic":1,
            "isfriend":0,
            "isfamily":0
        ]
        let flickPhoto = FlickrPhoto.createPhoto(fromJson: json)
        XCTAssertNotNil(flickPhoto)
        XCTAssertEqual("30466337097", flickPhoto?.id)
        XCTAssertEqual("f4dcdc92a8", flickPhoto?.secret)
        XCTAssertEqual("1921", flickPhoto?.server)
        XCTAssertEqual(2, flickPhoto?.farm)

    }

}
